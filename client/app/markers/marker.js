//Handles button click events
  Template.body.events({
    'click .upvotebutton': function (e) {
      e.preventDefault();

      console.log(this.id);
      Meteor.call('upvote', this.id, (err, response)=>{
        if(err) {
          Session.set('serverDataResponse', "Error:" + err.reason);
          return;
        }
        Session.set('serverDataResponse', response);
      }); 
    },
    'click .downvotebutton': function (e) {
      e.preventDefault();
      Meteor.call('downvote', this.marker.id, (err, response)=>{
        if(err) {
          Session.set('serverDataResponse', "Error:" + err.reason);
          return;
        }
        Session.set('serverDataResponse', response);
      }); 
    },
    'click .addMarker': function (e) {
      e.preventDefault();
      selectedPlace = Session.get('selectedPlace');

      Meteor.call('addMarker', selectedPlace, (err, response)=>{
        if(err) {
          Session.set('serverDataResponse', "Error:" + err.reason);
          return;
        }
        Session.set('serverDataResponse', response);
      }); 
    },
    'click .removeMarker': function (e) {
      e.preventDefault();
      Meteor.call('removeMarker', this.id, (err, response)=>{
        if(err) {
          Session.set('serverDataResponse', "Error:" + err.reason);
          return;
        }
        Session.set('serverDataResponse', response);
      }); 
    }
  });